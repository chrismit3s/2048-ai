from argparse import ArgumentParser
from os.path import join
from shutil import rmtree
from src.game import game, MOVE_LIST
from src.neuralnet import Agent
from time import strptime, sleep
import numpy as np
import pygame as pg


def train(game, file, episodes, end_time, discount, eps, decay, save):
    # try to convert the file arg to an int
    try: 
        file = int(file)
    except ValueError:
        if file == "":
            file = None

    # create agent
    agent = Agent(game=game, load_file=file)

    # train till finish or ctrl c
    agent.main_model.summary(print_fn=lambda x, *a, **ka: print(x, *a, **ka) if set(x) != {"_"} else None)
    try:
        if episodes != 0:
            agent.train_for(episodes=episodes,
                            discount=discount,
                            eps=eps,
                            eps_decay=decay)
        elif end_time != "":
            agent.train_until(end=strptime(end_time, "%Y-%m-%dT%H:%M:%S"),
                              discount=discount,
                              eps=eps,
                              eps_decay=decay)
        else:
            raise ValueError("Neither episodes nor end_time is set")
    except KeyboardInterrupt:
        # if save is set, ask if the model should be saved even though the training was cancelled
        if save:
            save = (input("\nSave model? (Y/n) ").lower() != "n")
    finally:
        if save:
            # save model
            agent.save()
        else:
            # delete logs
            rmtree(join("logs", f"model-{agent.creation_timestamp}"), ignore_errors=True)

    # print last field
    print("Last field:")
    agent.main_game.draw()

    # count last moves
    counter = np.bincount(list(x for x, _ in agent.move_hist if x != -1))
    print("Last moves:", ", ".join(f"{k}: {v}" for k, v in zip(MOVE_LIST, counter)))

    # check if all predictions were the same
    *_, x = agent.move_hist[0]
    for *_, y in list(agent.move_hist)[1:]:
        if not np.allclose(x, y):
            break
    else:
        print(f"Model is broken (overfit?), all preditions are {x}")

    # offer to print last moves
    if input("See the last 100 moves? (y/N) ").lower() == "y":
        for move, pred in agent.move_hist:
            print(pred, MOVE_LIST[move])

def play(game, file, tick):
    # try to convert the file arg to an int
    try: 
        file = int(file)
    except ValueError:
        if file == "":
            file = None

    # create agent
    agent = Agent(game=game, load_file=file)

    # play the game
    while not agent.main_game.is_end():
        # output field
        sleep(tick)
        agent.main_game.draw()

        # get and make move
        pred = agent.predict()
        move = agent.select_best(pred)
        print(pred, MOVE_LIST[move])
        agent.main_game.tick(move)

        # keep window responsive
        if pg.event.peek(pg.QUIT):
            exit()
    while not pg.event.peek(pg.QUIT):
        agent.main_game.draw()


# create parser
parser = ArgumentParser()
subparsers = parser.add_subparsers()

# train
train_parser = subparsers.add_parser("train")
train_mode_group = train_parser.add_mutually_exclusive_group(required=True)
train_mode_group.add_argument("--episodes", "-e",
                              type=int,
                              default=0,
                              help="the number of episodes the modes should be trained")
train_mode_group.add_argument("--end-time", "-t",
                              dest="end_time",
                              type=str,
                              default="",
                              help="the point in time when the training should stop")
train_parser.add_argument("--difficulty", "-d",
                          type=str,
                          default="normal",
                          choices=["easy", "normal", "hard", "evil"],
                          help="the difficulty of the training games")
train_parser.add_argument("--field-size", "-F",
                          dest="field_size",
                          type=int,
                          default=4,
                          help="the size of the game field")
train_parser.add_argument("--file", "-f",
                          type=str,
                          default="",
                          help="load this pretrained model before training")
train_parser.add_argument("--discount", "-y",
                          type=float,
                          default=0.95,
                          help="the y/discount value for reinforcement learning")
train_parser.add_argument("--eps",
                          type=float,
                          default=0.5,
                          help="the eps value for eps-greedy reinforcement learning")
train_parser.add_argument("--decay",
                          type=float,
                          default=0.999,
                          help="the eps decay value for eps-greedy reinforcement learning")
train_parser.add_argument("--dont-save", "-S",
                          dest="save",
                          action="store_false",
                          help="do a dry run and don't save the model after training")
train_parser.set_defaults(func=train, skin="terminal")

# play
play_parser = subparsers.add_parser("play")
play_parser.add_argument("--skin", "-s",
                         type=str,
                         default="default",
                         choices=["custom", "default", "terminal", "base"],
                         help="the colorscheme used in the output, if 'terminal' the show the output in the terminal, else a gui starts in a new window")
play_parser.add_argument("--difficulty", "-d",
                         type=str,
                         default="normal",
                         choices=["easy", "normal", "hard", "evil"],
                         help="the difficulty of the training games")
play_parser.add_argument("--field-size", "-F",
                         dest="field_size",
                         type=int,
                         default=4,
                         help="the size of the game field")
play_parser.add_argument("--file", "-f",
                         type=str,
                         default="-1",
                         help="load this model to play, defaults to the most recent")
play_parser.add_argument("--tick", "-t",
                         type=float,
                         default=0.4,
                         help="the tick speed of the simulation")
play_parser.set_defaults(func=play)

# get args as dict
args = parser.parse_args()
func = args.func
args = {k: v for k, v in vars(args).items() if k is not "func"}

# split args into game and func args
keys = ["difficulty", "skin", "field_size"]
game_args = {k: v for k, v in args.items() if k in keys}
func_args = {k: v for k, v in args.items() if k not in keys}
func(game(**game_args), **func_args)
