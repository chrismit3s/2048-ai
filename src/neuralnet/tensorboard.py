from keras.callbacks import TensorBoard
import tensorflow as tf


# from https://pythonprogramming.net/deep-q-learning-dqn-reinforcement-learning-python-tutorial/
# and https://github.com/daniel-kukiela with slight modifications
class ModifiedTensorBoard(TensorBoard):
    def __init__(self, *args, **kwargs):
        # Overriding init to set initial step and writer (we want one log file for all .fit() calls)
        super().__init__(*args, **kwargs)
        self.step = 0
        self.writer = tf.summary.FileWriter(self.log_dir)

    def on_epoch_end(self, epoch, logs=None):
        self._write_logs(logs, self.step)

    def set_model(self, *args, **kwargs):
        pass

    def on_batch_end(self, *args, **kwargs):
        self.step += 1

    def on_train_end(self, *args, **kwargs):
        pass