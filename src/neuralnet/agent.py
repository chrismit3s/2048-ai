from collections import deque, namedtuple
from hashlib import sha256
from keras.layers import Dense, Conv2D, MaxPool2D, Flatten, Reshape
from keras.models import Sequential, load_model
from os import listdir
from os.path import join
from random import random, randrange, sample, choices
from src.game import Base2048, MOVE_LIST
from src.helpers import isotime
from src.neuralnet import NUM_CHANNELS, DATA_FORMAT, MINIBATCH_SIZE, MEMORY_SIZE, TARGET_UPDATE, ModifiedTensorBoard
from time import time, strftime, localtime, mktime, gmtime
import numpy as np
import os


Transition = namedtuple("Transition", ["tiles", "action", "reward"])


class Agent():
    def __init__(self, game=None, load_file=None):
        self.training_time = 0
        self.trained_episodes = 0
        self.main_game = game or Base2048()
        self.test_game = Base2048()
        self.replay_memory = deque(maxlen=MEMORY_SIZE)
        self.target_update_counter = 0
        self.creation_timestamp = isotime(use_seperators=False, use_timezone=True)
        self.tensorboard = ModifiedTensorBoard(log_dir=join("logs", f"model-{self.creation_timestamp}"))
        self.move_hist = deque(maxlen=100)

        # load/create a model
        if load_file is None:
            self.new()
        else:
            self.load(load_file)

        # vectorize normalization function for entire batches
        self.normalize_batch = np.vectorize(self.normalize, signature=f"{self.main_game.tiles.shape}->{self.get_input_shape()}".replace(" ", ""))

    def new(self):
        self.main_model = self.get_model()
        self.target_model = self.get_model()
        self.sync_models()

    def load(self, filename):
        if isinstance(filename, int):
            filename = join("models", sorted(x for x in os.listdir("models") if x.endswith(".h5"))[filename])
        self.main_model = load_model(filename)
        self.target_model = load_model(filename)

    def save(self):
        filename = join("models", f"model-{self.creation_timestamp}.h5")
        self.main_model.save(filename)

        # append summary to file
        with open(join("models", "model-info.txt"), "a+") as fh:
            fh.write(f"{filename.replace(os.linesep, '/')}{os.linesep}")
            self.main_model.summary(print_fn=lambda x, *a, **ka: fh.write(f"{x}{os.linesep}") if set(x) != {"_"} else None)  # skip lines only containing underscores
            fh.write(f"Input shape: {self.get_input_shape()}{os.linesep}")
            fh.write(f"Num channels: {NUM_CHANNELS}{os.linesep}")
            fh.write(f"Data format: {DATA_FORMAT}{os.linesep}")
            fh.write(f"Trained for: {isotime(self.training_time, show_date=False)}; {self.trained_episodes} episodes{os.linesep}")
            fh.write(f"Notes: /{os.linesep}")
            fh.write(f"{os.linesep}{os.linesep}")

    def get_model(self):
        acti_kw = {"activation": "selu",
                   "kernel_initializer": "lecun_normal",
                   "bias_initializer": "lecun_normal"}
        conv_kw = {"data_format": DATA_FORMAT,
                   "padding": "same"}

        model = Sequential()

        #model.add(Flatten(input_shape=self.get_input_shape()))
        #model.add(Dense(units=np.prod(self.get_input_shape()) // NUM_CHANNELS * 16, **acti_kw))
        #model.add(Reshape(target_shape=self.get_input_shape(16)))

        model.add(Conv2D(filters= 32, kernel_size=3, **conv_kw, **acti_kw, input_shape=self.get_input_shape()))
        model.add(Conv2D(filters= 64, kernel_size=3, **conv_kw, **acti_kw))
        model.add(Conv2D(filters=256, kernel_size=3, **conv_kw, **acti_kw))

        model.add(MaxPool2D(pool_size=2, strides=1, data_format=DATA_FORMAT, padding="valid"))
        model.add(Flatten())

        model.add(Dense(units=256, **acti_kw))
        model.add(Dense(units= 64, **acti_kw))
        model.add(Dense(units= 16, **acti_kw))
        model.add(Dense(units=  4))

        model.compile(loss="mse", optimizer="adam", metrics=["accuracy"])

        return model

    def sync_models(self):
        weights = self.main_model.get_weights()
        self.target_model.set_weights(weights)

    def get_input_shape(self, num_channels=None):
        num_channels = num_channels or NUM_CHANNELS
        if DATA_FORMAT == "channels_last":
            return (*self.main_game.tiles.shape, num_channels)
        else:
            return (num_channels, *self.main_game.tiles.shape)

    def select_best(self, weights):
        for move in np.argsort(weights)[::-1]:
            if self.main_game.is_valid_move(move):
                return move
        return -1

    def normalize(self, tiles):
        if NUM_CHANNELS > 1 and DATA_FORMAT == "channels_last":
            # one hot encodes the entire array (channels last), +1 for empty tiles which will be stripped next
            tiles = np.eye(NUM_CHANNELS + 1)[tiles + 1]

            # strip empty tiles
            tiles = tiles[:, :, 1:]
        elif NUM_CHANNELS == 1:
            tiles = 1 - 0.5 ** tiles
            tiles = tiles[np.newaxis, ...] if DATA_FORMAT == "channels_first" else tiles[..., np.newaxis]
        return tiles

    def predict(self, x=None, *args, model: {"main", "target"} = "main", **kwargs):
        """
        use the specified model to predict the expected rewards for
        either an entire batch of inputs or a single tile array (by
        default, the current game tiles)
        """

        # either normalize batch or just the single input array
        x = self.main_game.tiles if (x is None) else x
        if len(x.shape) == 2:
            # add batch dim
            x = self.normalize(x)[np.newaxis, ...]
        else:
            # normalize input array
            x = self.normalize_batch(x)

        # make prediction
        model = self.main_model if (model == "main") else self.target_model
        pred = model.predict(x, *args, **kwargs)

        # remove batch dim if x was single input array
        if pred.shape[0] == 1:
            pred = pred[0]

        return pred
    
    def fit(self, discount, *args, **kwargs):
        # only train if there's enough data
        if len(self.replay_memory) < 4 * MINIBATCH_SIZE:
            return

        # get training batches
        transitions = sample(self.replay_memory, k=MINIBATCH_SIZE)

        x = []
        y = []
        for tiles, action, reward in transitions:
            # append inputs
            x.append(self.normalize(tiles))

            # get average reward over all possible next tiles
            next_reward = 0
            for next_tiles, prob in self.main_game.iter_next_tiles():
                next_reward += prob * np.max(self.predict(next_tiles, model="target"))

            # get prediction vector and fix action-reward
            pred = self.predict(tiles, model="main")
            pred[action] = reward + discount * next_reward

            # append corrected outputs
            y.append(pred)

        self.main_model.fit(np.asarray(x),
                            np.asarray(y),
                            *args,
                            batch_size=MINIBATCH_SIZE,
                            callbacks=[self.tensorboard],
                            **kwargs)

    def train_for(self, episodes, discount=0.95, eps=0.5, eps_decay=0.999):
        t = time()
        previous_rewards = deque(maxlen=20)
        lines = ["" for _ in range(3)]
        print(f"Training start: {isotime(use_timezone=True)}")
        print("Training progress: 0.00%", end="\r", flush=True)
        for i in range(episodes):
            # update counters
            self.trained_episodes += 1
            self.training_time = time() - t

            # train one episode
            previous_rewards.append(self.episode(discount, eps))
            eps *= eps_decay

            # display progress
            remaining = (time() - t) * (episodes - i) / (i + 1)
            average_reward = sum(previous_rewards) / len(previous_rewards)
            lines[0] = f"{100 * i / episodes:.2f}%"
            lines[1] = f"{remaining / 3600:02.0f}:{strftime('%M:%S', gmtime(remaining))} remaining (until {isotime(remaining + mktime(localtime()), use_timezone=True)})"
            lines[2] = f"average reward {average_reward:.0f}"
            print("Training progress:", "; ".join(lines), end="\r", flush=True)
        print(f"Training end: {isotime(use_timezone=True)}")

    def train_until(self, end, discount=0.95, eps=0.5, eps_decay=0.999):
        t = time()
        previous_rewards = deque(maxlen=20)
        lines = ["" for _ in range(3)]
        print(f"Training start: {isotime(use_timezone=True)}")
        print("Training progress: 0.00%", end="\r", flush=True)
        while localtime() < end:
            # update counters
            self.trained_episodes += 1
            self.training_time = time() - t

            # train one episode
            previous_rewards.append(self.episode(discount, eps))
            eps *= eps_decay

            # display progress
            remaining = mktime(end) - mktime(localtime())
            average_reward = sum(previous_rewards) / len(previous_rewards)
            lines[0] = f"{100 * (mktime(localtime()) - t) / (mktime(end) - t):.2f}%"
            lines[1] = f"{remaining / 3600:02.0f}:{strftime('%M:%S', gmtime(remaining))} remaining (until {isotime(mktime(end), use_timezone=True)})"
            lines[2] = f"average reward {average_reward:.0f}"
            print("Training progress:", "; ".join(lines), end="\r", flush=True)
        print(f"Training end: {isotime(use_timezone=True)}")

    def episode(self, discount, eps):
        self.main_game.reset()
        total_reward = 0
        while not self.main_game.is_end():
            transition_dict = dict()

            # choose reward-maximizing or random action
            pred = self.predict(model="target")
            move = self.select_best(pred) if (random() > eps) else randrange(4)

            # check if there was a valid move
            if move == -1:
                break

            # save it and the game state
            self.move_hist.append((move, pred))
            transition_dict["action"] = move
            transition_dict["tiles"] = self.main_game.tiles.copy()


            # take action
            prev_score = self.main_game.score
            valid_move = self.main_game.tick(move)

            # compute score delta and save reward
            reward = self.main_game.score - prev_score if valid_move else -1
            total_reward += reward
            transition_dict["reward"] = reward

            # make the transition a tuple and append it to the queue
            self.replay_memory.append(Transition(**transition_dict))

            # train
            self.fit(discount=discount, verbose=0)

            # sync the models
            self.target_update_counter += 1
            if self.target_update_counter == TARGET_UPDATE:
                self.sync_models()
                self.target_update_counter = 0

        return total_reward
