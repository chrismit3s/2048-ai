from numpy import array
import pygame as pg
import os
from time import localtime, gmtime, strftime


def isotime(t=None, show_date=True, show_time=True, use_seperators=True, use_timezone=False):
    f = ""
    f += ("%Y-%m-%d" if show_date else "").replace("-" if not use_seperators else "", "")
    f += "T" if show_date and show_time else ""
    f += ("%H:%M:%S" if show_time else "").replace(":" if not use_seperators else "", "")
    return strftime(f, localtime(t) if use_timezone else gmtime(t))

def draw_tile(surface, fg_color, bg_color,
              rect, text,
              font="calibri",
              margin=1,
              max_size=20,
              radius=10):
    # draw tile
    round_rect(surface=surface,
               color=bg_color,
               rect=rect,
               radius=radius)

    # draw text
    text_rect(surface=surface,
              color=fg_color,
              rect=rect,
              text=text,
              font=font,
              max_size=max_size,
              margin=margin)

def text_rect(surface, color, rect, text, font="calibri", margin=1, max_size=20):
    left, top, width, height = array(rect).flatten()

    # is there even text
    if text == "":
        return

    # scale for ascent
    font_obj = pg.font.SysFont(font, int(max_size))
    max_size *= min(1, height / font_obj.get_ascent())

    # scale for bounding rect
    font_obj = pg.font.SysFont(font, int(max_size))
    rect_size = margin * array((width, height))
    text_size = array((font_obj.size(text)[0],
                       font_obj.get_ascent()))
    max_size *= min(1, *(rect_size / text_size))


    # get text rect and move center slightly off
    font_obj = pg.font.SysFont(font, int(max_size))
    text_obj = font_obj.render(text, True, color)
    text_rect = text_obj.get_rect()
    text_rect.center = (left + width // 2,
                        top + height // 2 - (font_obj.get_descent() // 2 if font != "arial" else 0))  # font specific fix

    # draw text
    surface.blit(text_obj, text_rect)

def round_rect(surface, color, rect, radius):
    left, top, width, height = array(rect).flatten()
    right = left + width
    bottom = top + height
    corners = [(left,  top),
               (right, top),
               (right, bottom),
               (left,  bottom)]

    # draw polygon with canted corners
    points = []
    direction = 2  # offset direction to keep track in which direction the offset should go (up = 0, increments clockwise)
    for x, y in corners:
        for a in [0, 1]:
            points.append((x + (0 if (direction % 2 == 0) else 1) * (1 if (direction // 2 == 0) else -1) * radius,
                           y + (0 if (direction % 2 == 1) else 1) * (1 if (direction // 2 == 1) else -1) * radius))
            direction -= (1 + a)
            direction %= 4
    pg.draw.polygon(surface, color, points)

    # draw circles over canted corners
    for x in [left + radius, left + width - radius]:
        for y in [top + radius, top + height - radius]:
            pg.draw.circle(surface, color, (x, y), radius)
