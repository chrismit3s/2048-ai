from src.game import BARS, Base2048


class Terminal2048(Base2048):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.key_dict = dict(zip(("w", "a", "s", "d"),
                                 range(4)))

    def get_move(self):
        return self.key_dict[input().lower()[0]]

    def draw(self):
        # print score
        print("Score:", self.score)

        # print grid
        for r in range(self.tiles.shape[0]):
            for c in range(self.tiles.shape[1]):
                print(f"{self.tiles[r, c] if (self.tiles[r, c] != -1) else ' ':2}", end="")
                print(BARS["vertical"] if (c + 1 < self.tiles.shape[1]) else "\n", end="")
            if r + 1 < self.tiles.shape[0]:
                for c in range(self.tiles.shape[1]):
                    print(BARS["horizontal"] * 2, end="")
                    print(BARS["cross"] if (c + 1 < self.tiles.shape[1]) else "\n", end="")
        print("")
        return
