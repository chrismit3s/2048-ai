from math import log
from random import random, choice
import numpy as np


class Base2048():
    def __init__(self, difficulty: {"easy", "normal", "hard"} = "normal", rows=4, cols=4):
        self.difficulty = difficulty
        self.tile_gen = {
            "easy":   lambda: 0,
            "normal": lambda: 0 if (random() > 0.1) else 1,
            "hard":   lambda: min(int(log(random(), 0.5)),
                                  max(np.max(self.tiles), 0))}
        self.tiles = np.full(shape=(rows, cols),
                             fill_value=-1,
                             dtype="int8")
        self.reset()

    def reset(self):
        self.score = 0
        self.tiles = np.full(shape=self.tiles.shape,
                             fill_value=-1,
                             dtype="int8")
        self.add_tile()
        self.add_tile()

    def add_tile(self):
        # get list of free tiles
        indices = np.argwhere(self.tiles == -1)
        if len(indices) == 0:
            return

        # choose random index
        index = tuple(choice(indices))

        # fill with value
        self.tiles[index] = self.tile_gen[self.difficulty]()

    def iter_next_tiles(self):
        """
        yields   a  tuple  for  ever   possible  new  tile  with  its
        probability
        """
        # get list of free tiles
        indices = np.argwhere(self.tiles == -1)
        length = len(indices)
        if length == 0:
            return

        for index in indices:
            for i in range(2):
                copy = self.tiles.copy()
                copy[index] = i
                yield copy, (0.9 if (i == 0) else 0.1) / length

    def free_tiles(self):
        return np.bincount(self.tiles.flatten() + 1)[0]

    def highest_tile(self):
        return np.max(self.tiles)

    def get_lr_view(self, direction: int, tiles=None):
        """
        creates tile view of the game tiles or the tiles specified in
        which the move is always from right to left, all changes made
        to this view are also made to the game tiles

        e.g:  [[0 . 1]     [[0 . 2] 
               [. ^ .]  ->  [. < .]
               [2 . 3]]     [1 . 3]]

              [[0 . 1]     [[1 . 0] 
               [. > .]  ->  [. < .]
               [2 . 3]]     [3 . 2]]
        """
        tiles = np.asarray(self.tiles if (tiles is None) else tiles)
        tile_view = tiles.view()

        # map up/down to left/right
        if direction % 2 == 0:
            tile_view = tile_view.transpose()

        # map right(/down; which has previously been mapped to right) to left
        if direction // 2 == 1:
            tile_view = tile_view[:, ::-1]

        return tile_view

    def slide(self, direction: int):
        def _slide_line(line):
            j = None  # index of previous non empty tile
            for i, tile in enumerate(line):
                # skip empty tiles
                if tile == -1:
                    continue

                # initialize j on the first non empty tile (after merge)
                if j is None:
                    j = i
                    continue

                # find two consecutive, non-empty tiles with the same value
                if line[j] == line[i]:
                    # remove one tile and increase the other
                    line[j] = -1
                    line[i] += 1
                    self.score += 2 ** (line[i] + 1)
                    j = None
                else:
                    j = i

            return sorted(line,
                          key=(lambda x: min(0, x)),  # -1 if tiles is -1, 0 elsewise
                          reverse=True)

        # create tile view in which the move is always from left to right
        view = self.get_lr_view(direction)
        view[:, :] = np.apply_along_axis(func1d=_slide_line,
                                         axis=1,
                                         arr=view)[:, :]

    def possible_merges(self, direction: int, tiles=None):
        """
        checks for all possilbe merges in tiles in the orientation
        specified, returns a numpy bool array with each value
        corresponding to a pair in the orientation
        """
        tiles = np.asarray(self.tiles if (tiles is None) else tiles)
        return (((tiles[:-1] == tiles[1:]) & (tiles[1:] != -1))
                if (direction % 2 == 1)
                else ((tiles[:, :-1] == tiles[:, 1:]) & (tiles[:, 1:] != -1)))

    def is_end(self):
        """
        returns True if the game is over and no move is valid, else
        False
        """
        # check if there are free tiles
        if self.free_tiles() != 0:
            return False

        # check for two adjacent tiles with the same value
        if self.possible_merges(0).any() or self.possible_merges(1).any():
            return False

        # all tiles are set, and no adjacent tiles have the same value => game is over
        return True

    def is_valid_move(self, direction: int):
        """
        returns True when the move changes something and a new tile
        would be added, else False
        """
        def _is_valid_move(line):
            for i in range(len(line) - 1):
                if (line[i] == -1 and line[i + 1] != -1) or (line[i] == line[i + 1] != -1):
                    return True
            return False

        view = self.get_lr_view(direction)
        return np.apply_along_axis(func1d=_is_valid_move,
                                   axis=1,
                                   arr=view).any()

    def get_move(self):
        raise NotImplementedError

    def draw(self):
        raise NotImplementedError

    def tick(self, direction: int):
        """
        ticks the game one state forwards, returns True if the move
        was valid, False elsewise
        """
        if self.is_valid_move(direction):
            self.slide(direction)
            self.add_tile()
            return True
        return False

    def loop(self):
        while not self.is_end():
            # output field
            self.draw()

            # get and make move
            self.tick(self.get_move())

        self.draw()
        self.get_move()
        return (self.score, self.highest_tile())
