from colorsys import hsv_to_rgb
from src.game.graphical import Graphical2048
import numpy as np


class Default2048(Graphical2048):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.colors = [(0xEE, 0xE4, 0xDA), (0xED, 0xE0, 0xC8),
                       (0xF2, 0xB1, 0x79), (0xF5, 0x95, 0x63),
                       (0xF6, 0x7C, 0x5F), (0xF6, 0x5E, 0x3B),
                       (0xED, 0xCF, 0x72), (0xED, 0xCC, 0x61),
                       (0xED, 0xC8, 0x50), (0xED, 0xC5, 0x3F),
                       (0xED, 0xC2, 0x2E), (0x00, 0x00, 0x00)]

    def __deepcopy__(self, memo):
        copy = super().__deepcopy__(memo)
        copy.colors = self.colors
        return copy

    def get_text(self, r, c):
        return "" if self.tiles[r, c] == -1 else str(2 ** (self.tiles[r, c] + 1))

    def get_tile_color(self, r, c):
        return self.empty_tile if self.tiles[r, c] == -1 else self.colors[min(self.tiles[r, c], len(self.colors) - 1)]
        
    def get_text_color(self, r, c):
        return (0x76, 0x6E, 0x65) if self.tiles[r, c] <= 1 else (0xF9, 0xF6, 0xF2)


class Custom2048(Graphical2048):
    def get_text(self, r, c):
        return "" if self.tiles[r, c] == -1 else str(self.tiles[r, c])

    def get_tile_color(self, r, c):
        hsv = np.array([(2 - 0.7 ** self.tiles[r, c]) / 3, 0.5, 1.0])
        rgb = np.array(hsv_to_rgb(*hsv))
        return self.empty_tile if self.tiles[r, c] == -1 else (255 * rgb).astype(int)
        
    def get_text_color(self, r, c):
        return (0x76, 0x6E, 0x65)
