GRID_RATIO = 0.1  # ratio of gird bar to tile
PX_PER_TILE = 100  # pixels per tile
BARS = {"vertical":   u"\u2502",
        "horizontal": u"\u2500",
        "cross":      u"\u253c"}
MOVE_LIST = ["up", "left", "down", "right"]


from src import helpers
from src.game.basegame import Base2048
from src.game.graphical import Graphical2048
from src.game.skins import Custom2048, Default2048
from src.game.terminal import Terminal2048


def game(skin: {"base", "terminal", "graphical", "default", "custom"} = "default",
         difficulty: {"easy", "normal", "hard"} = "normal",
         field_size=(4, 4)):
    # convert field to tuple
    if isinstance(field_size, int):
        field_size = 2 * (field_size,)

    kwargs = {"rows": field_size[0],
              "cols": field_size[1],
              "difficulty": difficulty}
    return eval(skin.capitalize() + "2048" + "(**kwargs)")
