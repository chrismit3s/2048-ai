from src.game import GRID_RATIO, PX_PER_TILE, Base2048
from src.helpers import draw_tile, text_rect, round_rect
from time import sleep
import pygame as pg
import numpy as np


class Graphical2048(Base2048):
    def __init__(self, *args, skin: {"default", "custom"}="default", **kwargs):
        super().__init__(*args, **kwargs)
        self.font_name = "futuramdbt"
        self.background = (0xBB, 0xAD, 0xA0)
        self.empty_tile = (0xCC, 0xBF, 0xB3)
        self.events = [pg.KEYDOWN, pg.QUIT]
        self.key_dict = dict(zip((pg.K_w, pg.K_a, pg.K_s, pg.K_d),
                                 range(4)))

        # everything for the gui
        self.tile_size = int(PX_PER_TILE)
        self.grid_size = int(PX_PER_TILE * GRID_RATIO)

        # get display (if wanted)
        pg.init()
        self.screen = pg.display.set_mode(self.get_tile_pos(*self.tiles.shape))

    def __deepcopy__(self, memo):
        copy = Graphical2048()
        copy.tiles      = self.tiles.copy()
        copy.key_dict   = self.key_dict.copy()
        copy.score      = self.score
        copy.difficulty = self.difficulty
        copy.font_name  = self.font_name
        copy.background = self.background
        copy.empty_tile = self.empty_tile
        return copy

    def get_text(self, r, c):
        return str(self.tiles[r, c])

    def get_tile_color(self, r, c):
        return self.empty_tile if self.tiles[r, c] == -1 else (0x22, 0x22, 0x22)
        
    def get_text_color(self, r, c):
        return (0xE0, 0xE0, 0xE0)
        
    def get_tile_pos(self, r, c):
        return (np.array((c, r)) * (self.grid_size + self.tile_size) + (self.grid_size, 2 * self.grid_size + self.tile_size)).astype(int)

    def get_tile_rect(self, r, c):
        return np.array((self.get_tile_pos(r, c), 2 * (self.tile_size,)), dtype=int)

    def draw_tile(self, r, c, x=None, y=None):
        draw_tile(surface=self.screen,
                  fg_color=self.get_text_color(r, c),
                  bg_color=self.get_tile_color(r, c),
                  rect=self.get_tile_rect(x or r, y or c),
                  text=self.get_text(r, c),
                  font=self.font_name,
                  max_size=self.tile_size,
                  margin=0.9,
                  radius=self.grid_size // 2)

    def draw_background(self):
        self.screen.fill(color=self.background)

    def draw_tiles(self):
        for index in np.ndindex(self.tiles.shape):
            self.draw_tile(*index)

    def draw_score(self):
        # get index of highest tile
        index = np.unravel_index(self.tiles.argmax(), self.tiles.shape)

        # draw tile
        rect = (self.grid_size,
                self.grid_size,
                self.tile_size + (self.tiles.shape[1] - 1) * (self.tile_size + self.grid_size),
                self.tile_size)
        draw_tile(surface=self.screen,
                  fg_color=self.get_text_color(*index),
                  bg_color=self.get_tile_color(*index),
                  rect=rect,
                  text=str(self.score),
                  font=self.font_name,
                  max_size=self.tile_size,
                  margin=0.9,
                  radius=self.grid_size // 2)

    def get_move(self):
        while True:
            event = pg.event.poll()
            if event.type not in self.events:
                continue

            if event.type == pg.KEYDOWN and event.key in self.key_dict:
                return self.key_dict[event.key]
            elif event.type == pg.QUIT:
                exit()

    def tick(self,
             direction: int,
             *args, **kwargs):
        super().tick(direction, *args, **kwargs)
        #self.animate(direction)

    def draw(self):
        self.draw_background()
        self.draw_score()
        self.draw_tiles()
        pg.display.flip()

    #def animate(self, direction: int, old, new=None):
    def animate(self, direction: int, steps=20):
        pos  = np.array((self.grid_size,
                         self.grid_size * 2 + self.tile_size),
                        dtype=float)
        size = np.array((self.tile_size + (self.tiles.shape[1] - 1) * (self.tile_size + self.grid_size),
                         self.tile_size),
                        dtype=float)
        step = np.array((0,
                         self.tile_size + (self.tiles.shape[1] - 1) * (self.tile_size + self.grid_size)),
                        dtype=float) / steps

        # step horizonal or vertical
        if direction % 2 == 1:
            size = size[::-1]
            step = step[::-1]

        # step forwards or backwards
        if direction // 2 == 0:
            step *= -1
            pos[min(direction, 1)] += (self.tiles.shape[1] - 1) * (self.tile_size + self.grid_size)

        for _ in range(steps):
            # check for relevant events
            if pg.event.peek(self.events):
                break

            # draw background (duh)
            self.draw_background()

            # little animation rectangle swooping over the screen
            round_rect(surface=self.screen,
                       color=self.empty_tile,
                       rect=np.array((pos, size), dtype=int),
                       radius=self.grid_size // 2)

            # draw the rest and flip the screen
            self.draw_score()
            self.draw_tiles()
            pg.display.flip()

            # increate the position
            pos += step
