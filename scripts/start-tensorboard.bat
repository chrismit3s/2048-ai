@echo off



REM path + venv setup
call "venv\scripts\activate.bat" >nul 2>&1



start http://127.0.0.1:1337/

tensorboard --logdir ./logs --host 127.0.0.1 --port 1337